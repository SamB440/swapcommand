package net.islandearth.swapcommand.listener;

import net.islandearth.swapcommand.SwapCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;

public record SwapHandListener(SwapCommand plugin) implements Listener {
    
    @EventHandler
    public void onSwap(PlayerSwapHandItemsEvent event) {
        Player player = event.getPlayer();
        String command;
        if (!player.isSneaking()) {
            command = plugin.getConfig().getString("command");
        } else command = plugin.getConfig().getString("shift_command");

        if (command == null) return;
        player.performCommand(command);
        if (plugin.getConfig().getBoolean("cancel_swap")) event.setCancelled(true);
    }
}
