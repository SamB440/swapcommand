package net.islandearth.swapcommand.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import net.islandearth.swapcommand.SwapCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

@CommandAlias("swapreload")
public class SwapReloadCommand extends BaseCommand {
    
    private final SwapCommand plugin;
    
    public SwapReloadCommand(SwapCommand plugin) {
        this.plugin = plugin;
    }
    
    @Default
    public void onDefault(CommandSender sender) {
        plugin.reloadConfig();
        sender.sendMessage(ChatColor.GREEN + "Configuration reloaded.");
    }
}
