package net.islandearth.swapcommand;

import co.aikar.commands.PaperCommandManager;
import net.islandearth.swapcommand.command.SwapReloadCommand;
import net.islandearth.swapcommand.listener.SwapHandListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class SwapCommand extends JavaPlugin {
    
    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        this.registerCommands();
        this.registerEvents();
    }
    
    private void registerCommands() {
        PaperCommandManager manager = new PaperCommandManager(this);
        manager.registerCommand(new SwapReloadCommand(this));
    }
    
    private void registerEvents() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new SwapHandListener(this), this);
    }
}
